#include "BLEDevice.h"
#include "VescESP32.h"

//Config
#define MAC_ADDRESS "Mac Address of the BLE device you want to connect to"
const uint32_t PIN = 123456; //Pin code
BLEUUID serviceUUID("0000ffe0-0000-1000-8000-00805f9b34fb");
BLEUUID charUUID("0000ffe1-0000-1000-8000-00805f9b34fb");

///////////////////////////////////////////////////////////////////////////////
VescESP32 controller(serviceUUID, charUUID, charUUID);

class SecurityHandler : public BLESecurityCallbacks {
    uint32_t onPassKeyRequest(){
        Serial.printf("Recevied pin code request from ESK8");
        return PIN;
    }

    bool onConfirmPIN(uint32_t pin){
        return true;
    }

    bool onSecurityRequest(){
        return true;
    }
    void onPassKeyNotify(uint32_t key){}

    void onAuthenticationComplete(esp_ble_auth_cmpl_t auth_cmpl){
        if(auth_cmpl.success){
            Serial.printf("AuthenticationComplete\n");
        }else{
            Serial.printf("Wrong passcode!\n");
        }
    }
};

void TxCallback(BLERemoteCharacteristic *remote_char, uint8_t *data, size_t length, bool notified){
    Serial.println("DATA INCOME");
    for(auto i = 0; i < length; i++){
        Serial.printf("%d ", data[i]);
    }
    Serial.printf("DATA END\n");
}


void setup(){
    Serial.begin(115200);
    Serial.println("Initializing...");
    controller.begin(MAC_ADDRESS);
    controller.setDebugPort(&Serial);
    controller.setOnRecieveCallback(TxCallback);
    Serial.println("done");
}

void loop(){
    Serial.println("update");
    //Wait for connection
    if(!controller.available()){
        Serial.println("Attemptting to connect...");
        auto result = controller.connectWithSecurity(new SecurityHandler());
        if(!result){
            Serial.println("Fail to connect");
            controller.disconnect();
        }else{
            Serial.println("Successfully connected");
        }
    }else{
        Serial.println("Sending request");
        uint8_t cmd[1] = {COMM_FW_VERSION};
        controller.request(cmd, 1);
        delay(1000);
    }
}
