#pragma once
#include <Arduino.h>
#include "BLEDevice.h"
#include "datatypes.h"

typedef struct {
    float mos_temp;
    float motor_temp;
    double motor_current;
    double input_current;
    double id;
    double iq;
    float current_duty;
    double rpm;
    float input_voltage;
    double amp_hours;
    double amp_hours_charged;
    double watt_hours;
    double watt_hours_charged;
    uint32_t tachometer;
    uint32_t abs_tachometer;
} VescDataPacket; 

class VescESP32{
    public:
        VescESP32(BLEUUID serviceUUID, BLEUUID txUUID, BLEUUID rxUUID);
        void begin(char *address);
        bool connectWithSecurity(BLESecurityCallbacks *security_cb);
        bool connect();
        void disconnect();
        int available();
        void request(uint8_t *payload, uint16_t length);
        void setCurrent(float amp);
        void setDuty(float duty);
        int fetchData(uint8_t *buffer);
        void setDebugPort(HardwareSerial *port);

    private:
        static void _onRecieveCallback(BLERemoteCharacteristic *remote_char, uint8_t *data, size_t length, bool notified);
        VescDataPacket data_packet;
        BLEUUID service_uuid;
        BLEUUID tx_uuid;
        BLEUUID rx_uuid;
        BLEAddress *addr;
        BLEClient *client;
        BLERemoteCharacteristic *tx_char;
        BLERemoteCharacteristic *rx_char;
        bool same_uuid = false;
        HardwareSerial *debug;
};