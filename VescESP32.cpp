#include "VescESP32.h"
#include "crc.h"

VescESP32::VescESP32(BLEUUID serviceUUID, BLEUUID txUUID, BLEUUID rxUUID){
    service_uuid = serviceUUID;
    //if tx and rx use the same characteristic
    same_uuid = txUUID.equals(rxUUID) ? true : false;
    tx_uuid = txUUID;
    rx_uuid = rxUUID;
}

void VescESP32::begin(char *address){
    addr = new BLEAddress(address);
    if(BLEDevice::getInitialized()){
        BLEDevice::deinit(true);
    }
    BLEDevice::init("");
    client = BLEDevice::createClient();
}


void VescESP32::_onRecieveCallback(BLERemoteCharacteristic *remote_char, uint8_t *data, size_t length, bool notified){
    static uint16_t count = 0;
    static uint16_t given_length = 0;
    static bool recieving;
    static uint8_t buffer[256];
    static uint32_t timeout = 0;
    //new data packet
    if(!recieving){
        if(data[0] == 2){
            given_length = (uint16_t)data[1] + 5;
        }
        count = 0;
        recieving = true;
    }else{
        timeout += millis();
    }
    //continue recieving incoming data
    memcpy(&buffer[count], data, length);
    count += length;

    if(count >= given_length){
        for(auto i = 0; i < count; i++){
            Serial.printf("%x ", buffer[i]);
        }
        // check stop byte
        if(buffer[count-1] == 3){
            //process complete packet
            uint16_t carried_crc = (uint16_t)((buffer[count-3] << 8) | buffer[count-2]);
            uint16_t package_crc = crc16(&buffer[2], buffer[1]);
            if(package_crc == carried_crc){
                // valid packet
                Serial.printf("\nSecurity passed\n");
            }
        }
        recieving = false;
    }
    //timeout
    if(timeout > 300){
        recieving = false;
    }
}

bool VescESP32::connectWithSecurity(BLESecurityCallbacks *security_cb){
    BLEDevice::setEncryptionLevel(ESP_BLE_SEC_ENCRYPT);
    BLEDevice::setSecurityCallbacks(security_cb);
    auto security = new BLESecurity();
    security->setAuthenticationMode(ESP_LE_AUTH_BOND);
    security->setCapability(ESP_IO_CAP_KBDISP);
    security->setRespEncryptionKey(ESP_BLE_ENC_KEY_MASK | ESP_BLE_ID_KEY_MASK);
    return connect();
}

bool VescESP32::connect(){
    if(client->connect(*addr)){
        debug->println("Connected to device.");
    }else{
        return false;
    }

    //check are the connected device contain the characteristic we need
    BLERemoteService *service = client->getService(service_uuid);
    if(!service){
        debug->println("The server cannot provide our wanted service");
        return false;
    }
    
    tx_char = service->getCharacteristic(tx_uuid);
    if(!tx_char){
        debug->println("Target device did not have given tx characteristic");
        return false;
    }

    if(!same_uuid){
        rx_char = service->getCharacteristic(rx_uuid);
        if(!rx_char){
            debug->println("Target device did not have given rx characteristic");
            return false;
        }
        rx_char->registerForNotify(_onRecieveCallback);
    }else{
        tx_char->registerForNotify(_onRecieveCallback);
    }
    return true;
}

void VescESP32::request(uint8_t *payload, uint16_t length){
    uint8_t packet[256];
    uint16_t checksum = crc16(payload, length);
    int idx = 0;
    if(length <= 256){
        packet[idx++] = 2;
        packet[idx++] = length;
    }else{
        packet[idx++] = 3;
        packet[idx++] = (uint8_t)(length >> 8);
        packet[idx++] = (uint8_t)(length & 0xff);
    }

    memcpy(&packet[idx], payload, length);
    idx += length;
    packet[idx++] = (uint8_t)(checksum >> 8);
    packet[idx++] = (uint8_t)(checksum & 0xff);
    packet[idx++] = 3;
    packet[idx] = 0;

    tx_char->writeValue(packet, idx);
}

void VescESP32::setCurrent(float amp){
    uint8_t cmd[5];
    uint32_t current = amp * 1000;
    cmd[0] = COMM_SET_CURRENT;
    cmd[1] = (uint8_t)(current >> 24);
    cmd[2] = (uint8_t)(current >> 16);
    cmd[3] = (uint8_t)(current >> 8);
    cmd[4] = (uint8_t)(current & 0xff);
    request(cmd, 5);
}

void VescESP32::setDuty(float duty){
    if(duty > 1){
        duty = 1.0;
    }
    uint8_t cmd[5];
    uint32_t duty_integer = duty * 100000;
    cmd[0] = COMM_SET_DUTY;
    cmd[1] = (uint8_t)(duty_integer >> 24);
    cmd[2] = (uint8_t)(duty_integer >> 16);
    cmd[3] = (uint8_t)(duty_integer >> 8);
    cmd[4] = (uint8_t)(duty_integer & 0xff);
    request(cmd, 5);
}

void VescESP32::disconnect(){
    if(client->isConnected()){
        client->disconnect();
    }
}

int VescESP32::available(){
    return (int)client->isConnected();
}

int VescESP32::fetchData(uint8_t *buffer){
    return 0;
}

void VescESP32::setDebugPort(HardwareSerial *port){
    debug = port;
}